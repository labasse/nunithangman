﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hangman
{
    public class HangmanDraw : IProgress<int>
    {
        private int _step = 0;

        public void Report(int value)
        {
            _step = value;
            Console.WriteLine(HangmanSteps[_step-1]);
        }
        public bool Ended => _step == HangmanSteps.Length;

        private readonly string[] HangmanSteps = new string[]
        {
@"+-----+
|
|
|
|
|
|\",
@"+-----+
|     |
|
|
|
|
|\",
@"+-----+
|     |
|     O
|
|
|
|\",
@"+-----+
|     |
|     O
|     |
|
|
|\",
@"+-----+
|     |
|     O
|    /|
|
|
|\",
@"+-----+
|     |
|     O
|    /|\
|
|
|\",
@"+-----+
|     |
|     O
|    /|\
|   _/
|
|\",
@"+-----+
|     |
|     O
|    /|\
|   _/ \_
|
|\"
        };
    }
}
