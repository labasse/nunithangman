﻿using System;
using System.Text;

namespace Hangman
{
    public class Hangman
    {
        private string _word;
        private StringBuilder _shown;
        private readonly bool [] _tries;
        private IProgress<int> _hangmanProgress;
        private int _hangmanValue = 0;
        private static int GetTryIndex(char letter) => letter - 'A';

        public Hangman(IWordDictionary wordDictionary, IProgress<int> hangmanProgress)
        {
            if(wordDictionary.Count<1)
            {
                throw new ArgumentException("Dictionary cannot be empty");
            }
            _word = wordDictionary.GetWord(
                TestableRandom.Next(wordDictionary.Count)
            );
            _hangmanProgress = hangmanProgress;
            _tries = new bool['Z' - 'A' + 1];
            _shown = new StringBuilder();
            for (int i = 0; i < _word.Length; i++)
            {
                _shown.Append('-');
            }
            Try(_word[0]);
        }

        public bool HasTried(char letter) => _tries[GetTryIndex(letter)];

        public bool AllLetterFound => FoundWord == _word;

        public string FoundWord => _shown.ToString();
        
        public bool Try(char letter)
        {
            if(letter<'A' || letter>'Z')
            {
                throw new ArgumentOutOfRangeException("Letter must between A and Z");
            }
            bool found = false;

            for(var i=0; i<_word.Length; i++)
            {
                if(_word[i]==letter)
                {
                    _shown[i] = letter;
                    found = true;
                }
            }
            var tryIndex = GetTryIndex(letter);

            if(!found && !_tries[tryIndex])
            {
                _hangmanProgress?.Report(++_hangmanValue);
            }
            _tries[tryIndex] = true;
            return found;
        }
    }
}
