﻿using System;

namespace Hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            var words = new WordDictionary(AllWords);
            var drawing = new HangmanDraw();
            var game = new Hangman(words, drawing); // Règles du jeu (à tester)

            while(!game.AllLetterFound && !drawing.Ended)
            {
                Console.WriteLine($"[ { game.FoundWord } ]");
                Console.Write("Type a letter : ");
                var key = Console.ReadKey();
                var letter = key.KeyChar;

                Console.WriteLine();
                if(letter < 'A' || letter > 'Z')
                {
                    Console.Error.WriteLine("Not a letter");
                }
                else
                {
                    game.Try(letter);

                    Console.Write("You tried : ");
                    for (char c = 'A'; c <= 'Z'; c++)
                    {
                        if (game.HasTried(c))
                        {
                            Console.Write($"{c} ");
                        }
                    }
                    Console.WriteLine();
                }
            }
        }

        private static readonly string[] AllWords = {
            "DANGEROUS",
            "GENERALLY",
            "NEWSPAPER",
            "STRUCTURE",
            "TELEPHONE",
            "WONDERFUL",
            "ATLANTIC",
            "BASEBALL",
            "BELIEVED",
            "CLOTHING",
            "FAVORITE",
            "FRIENDLY",
            "INDUSTRY",
            "INSTANCE",
            "POWERFUL",
            "RAILROAD",
            "WHATEVER",
            "WHENEVER",
            "BENEATH",
            "CHAPTER",
            "CHINESE",
            "CLEARLY",
            "CLIMATE",
            "CLOSELY",
            "GERMANY",
            "LEAVING",
            "NEAREST",
            "OBSERVE",
            "PACIFIC",
            "POPULAR",
            "RAPIDLY",
            "SERIOUS",
            "SERVICE",
            "SUPPORT",
            "WESTERN",
            "BEAUTY",
            "BELONG",
            "BIGGER",
            "BOTTLE",
            "COFFEE",
            "DETAIL",
            "DRIVER",
            "FLIGHT",
            "FLOWER",
            "FORGET",
            "FOURTH",
            "GERMAN",
            "GOLDEN",
            "HANDLE",
            "HEIGHT",
            "LEADER",
            "LIKELY",
            "MASTER",
            "MUSCLE",
            "NEARBY",
            "NOBODY",
            "PLENTY",
            "RUBBER",
            "SEEING",
            "SILENT",
            "SMOOTH",
            "SOURCE"
        };
    }
}
