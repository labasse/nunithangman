﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hangman
{
    public class TestableRandom
    {
        private static int? fake = null;
        private static Random rnd = new Random();

        public static int Next(int max)
        {
            if(fake.HasValue)
            {
                return fake.Value;
            }
            else
            {
                return rnd.Next(max);
            }
        }
        public static void SetFake(int fakeValue)
        {
            fake = fakeValue;
        }
    }
}
