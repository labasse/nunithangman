﻿using System.Collections.Generic;

namespace Hangman
{
    public class WordDictionary : IWordDictionary
    {
        private List<string> _words = new List<string>(); 

        public WordDictionary(IEnumerable<string> words)
        {
            _words.AddRange(words);
        }
        public int Count => _words.Count;

        public string GetWord(int index) => _words[index];
    }
}
