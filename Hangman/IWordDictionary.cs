﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hangman
{
    public interface IWordDictionary
    {
        int Count { get; }

        string GetWord(int index);
    }
}
