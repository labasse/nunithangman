using NUnit.Framework;
using System;
using System.Collections.Generic;
using H = Hangman;
using Moq;

namespace HangmanTest
{
    public class TestHangman
    {
        [SetUp]
        public void Setup()
        {
        }

        private H.Hangman NewHangmanWithLEADER()
        {
            var stubDic = new Mock<H.IWordDictionary>();

            H.TestableRandom.SetFake(3);
            stubDic.Setup(dic => dic.Count     ).Returns(2300);
            stubDic.Setup(dic => dic.GetWord(3)).Returns("LEADER");
            return new H.Hangman(stubDic.Object, null);
        }

        [Test]
        public void InitHangmanWithMinimumDictionary()
        {
            var test = NewHangmanWithLEADER();

            Assert.AreEqual( "L-----", test.FoundWord );
            Assert.IsFalse ( test.AllLetterFound ); // Better than Assert.AreEqual(false, test.AllLetterFound);
            Assert.IsTrue  ( test.HasTried('L') );
            Assert.IsFalse ( test.HasTried('E') );
            Assert.IsFalse ( test.HasTried('X') );
        }
        [Test]
        public void InitHangmanWithoutDictionary()
        {
            Assert.Throws<NullReferenceException>(()=>
                new H.Hangman(wordDictionary: null, hangmanProgress: null)
            );
        }
        [Test]
        public void InitHangmanWithEmptyDictionary()
        {
            Assert.Throws<ArgumentException>(() =>
                new H.Hangman(new H.WordDictionary(new string[] { }), null)
            );
        }
        [Test]
        public void TryALetterExistingOnceInTheWord()
        {
            var test = NewHangmanWithLEADER();
            var actual = test.Try('D');

            Assert.IsTrue (actual);
            Assert.AreEqual("L--D--", test.FoundWord);
            Assert.IsFalse(test.AllLetterFound);
            Assert.IsTrue (test.HasTried('D'));
        }
        [Test]
        public void TryALetterExistingTwiceInTheWord()
        {
            var test = NewHangmanWithLEADER();
            var actual = test.Try('E');

            Assert.IsTrue (actual);
            Assert.AreEqual("LE--E-", test.FoundWord);
            Assert.IsFalse(test.AllLetterFound);
            Assert.IsTrue (test.HasTried('E'));
        }
        [Test]
        public void TryALetterNotExistingInTheWord()
        {
            var test = NewHangmanWithLEADER();
            var actual = test.Try('X');

            Assert.IsFalse(actual);
            Assert.AreEqual("L-----", test.FoundWord);
            Assert.IsFalse(test.AllLetterFound);
            Assert.IsTrue (test.HasTried('X'));
        }
        [Test]
        public void TryAnAlreadyTriedLetterExistingInTheWord()
        {
            // Arrange
            var test = NewHangmanWithLEADER();

            test.Try('D');

            // Act
            var actual = test.Try('D');

            // Assert
            Assert.IsTrue(actual);
            Assert.AreEqual("L--D--", test.FoundWord);
            Assert.IsFalse(test.AllLetterFound);
            Assert.IsTrue(test.HasTried('D'));
        }
        [Test]
        public void TryAnAlreadyTriedLetterNotExistingInTheWord()
        {
            var test = NewHangmanWithLEADER();

            test.Try('X');

            var actual = test.Try('X');

            Assert.IsFalse(actual);
            Assert.AreEqual("L-----", test.FoundWord);
            Assert.IsFalse(test.AllLetterFound);
            Assert.IsTrue (test.HasTried('X'));
        }
        [Test]
        public void TryAndFindTheLastLetter()
        {
            var test = NewHangmanWithLEADER();

            test.Try('E');
            test.Try('D');
            test.Try('X');
            test.Try('R');

            var actual = test.Try('A');

            Assert.IsTrue(actual);
            Assert.AreEqual("LEADER", test.FoundWord);
            Assert.IsTrue(test.AllLetterFound);
        }
        [Test]
        public void TryAnInvalidCharacter()
        {
            var test = NewHangmanWithLEADER();

            Assert.Throws<ArgumentOutOfRangeException>(() =>
                test.Try('e')
            );
        }
    }
}